#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Copyright © 2019 Taylor C. Richberger
# This code is released under the license described in the LICENSE file

from pathlib import Path
from tempfile import TemporaryFile
from shutil import copyfileobj

class safe_write:
    '''Open a file for writing in a safe way, that rotates if no exception was caught'''
    def __init__(self, path, mode='w', *args, buffer_size=None, **kwargs):
        '''mode, args, and kwargs are passed through.

        buffer_size is the buffer size for the copy at the end.
        '''
        self.path = Path(path)
        self.mode = mode
        if 'b' in mode:
            self.tempmode = 'w+b'
        else:
            self.tempmode = 'w+'

        self.args = args
        self.kwargs = kwargs
        self.buffer_size = buffer_size

    def __enter__(self):
        self.file = TemporaryFile(mode=self.tempmode, dir=self.path.parent)
        return self.file.__enter__()

    def __exit__(self, exc_type, exc_value, traceback):
        if exc_value == None: 
            self.file.seek(0)
            with self.path.open(self.mode, *self.args, **self.kwargs) as file:
                length = []
                if self.buffer_size is not None:
                    length.append(self.buffer_size)
                copyfileobj(self.file, file, *length)

        return self.file.__exit__(exc_type, exc_value, traceback)
