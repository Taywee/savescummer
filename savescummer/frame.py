# -*- coding: utf-8 -*-
# Copyright © 2019 Taylor C. Richberger
# This code is released under the license described in the LICENSE file

from .panel import Panel
import wx

class Frame(wx.Frame):
    def __init__(self, app, title):
        super().__init__(None, title=title)
        self.title = title

        self.app = app
        self.panel = Panel(app, self)
        self.SetSize(self.DLG_UNIT(wx.Size(300,300)))
