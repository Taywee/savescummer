#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from . import _meta
from .app import App
import argparse
import locale
import sys
from pathlib import Path

def main():
    # A dumb hack to get around mac OSX stupid menu name generation
    sys.argv[0] = str(Path(sys.argv[0]).parent.parent / 'Save Scummer')

    app = App()
    app.MainLoop()

if __name__ == '__main__':
    main()
