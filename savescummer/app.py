#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Copyright © 2019 Taylor C. Richberger
# This code is released under the license described in the LICENSE file

from ._meta import version, about
from .frame import Frame
from io import TextIOWrapper
from pathlib import Path
from zipfile import ZipFile, ZIP_DEFLATED
import json
import gzip
import wx
from .file import safe_write

class App(wx.App):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.frame = Frame(
            app=self,
            title=f'Save Scummer version {version}',
        )
        self.frame.Show()

    def Close(self, force=False):
        self.frame.Close(force=force)
