# -*- coding: utf-8 -*-
# Copyright © 2019 Taylor C. Richberger and Brandon Phillips
# This code is released under the license described in the LICENSE file

from packaging.version import Version

version = Version('0.0.4')

data = dict(
    name='savescummer',
    version=str(version),
    author='Taylor C. Richberger',
    description='A simple program for savescumming in Noita',
    license='MIT',
    keywords='roguelike roguelite noita rpg game utility',
    url='https://gitlab.com/Taywee/savescummer',
    entry_points=dict(
        gui_scripts=[
            'savescummer = savescummer.__main__:main',
        ],
    ),
    packages=[
        'savescummer',
    ],
    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.6',
        'Operating System :: OS Independent',
        'License :: OSI Approved :: MIT License',
        'License :: OSI Approved :: MIT License',
        'Topic :: Utilities',
        'Topic :: Games/Entertainment',
    ],
)

_about = '''
This is Save Scummer, a free program for being savescum

This is free software.
'''

import re

_linebreakremover = re.compile(r'(\S)\n(\S)')

about = _linebreakremover.sub(r'\1 \2', _about.strip())
