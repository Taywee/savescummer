#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Copyright © 2019 Taylor C. Richberger
# This code is released under the license described in the LICENSE file

from .file import safe_write
from appdirs import AppDirs
from collections import namedtuple
from functools import lru_cache
from gettext import gettext as _
from pathlib import Path
from shutil import rmtree
from uuid import uuid4
import json
import os
import wx
import zipfile
import subprocess

class Slot:
    def __init__(self, notes, uuid=None):
        if uuid is None:
            uuid = str(uuid4())

        self.notes = notes
        self.uuid = uuid

class Panel(wx.Panel):
    def __init__(self, app, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.appdirs = AppDirs(
            appname='Noita Save Scummer',
            appauthor=False,
        )
        self.app = app

        if self.config_path().exists():
            print(f'pulling data from {self.config_path()}')
            with self.config_path().open() as file:
                data = json.load(file)
        else:
            exe = ''
            for path in [
                Path(r'C:\GOG Games\Noita\noita.exe'),
                Path(r'C:\Program Files (x86)\Steam\steamapps\common\Noita.exe'),
                Path(r'.\Noita.exe').resolve(),
            ]:
                if path.exists():
                    exe = str(path)
                    break
            if Path(r'C:\GOG Games\Noita\noita.exe').exists():
                exe = r'C:\GOG Games\Noita\noita.exe'
            data = {
                'exe': exe,
                'slots': {},
            }

        self.slots = {
            key: Slot(notes=slot['notes'], uuid=slot['uuid']) for key, slot in data['slots'].items()
        }

        top_sizer = wx.BoxSizer(wx.VERTICAL)

        exe_sizer = wx.StaticBoxSizer(wx.HORIZONTAL, self, _('Game Executable'))
        save_sizer = wx.StaticBoxSizer(wx.VERTICAL, self, _('Save Slots'))
        save_button_sizer = wx.BoxSizer(wx.HORIZONTAL)

        self.exe_textbox = wx.TextCtrl(self, value=data['exe'])
        exe_button = wx.Button(self, label=_('Select Noita Executable'))
        exe_sizer.Add(self.exe_textbox, wx.SizerFlags().DoubleBorder(wx.ALL).Proportion(1))
        exe_sizer.Add(exe_button)

        self.save_combobox = wx.ComboBox(self, choices=list(self.slots))
        save_notes_label = wx.StaticText(self, label=_('Notes'))
        self.save_notes = wx.TextCtrl(self, style=wx.TE_MULTILINE)
        save_button = wx.Button(self, label=_('Save'))
        load_button = wx.Button(self, label=_('Load'))
        delete_button = wx.Button(self, label=_('Delete'))

        save_sizer.Add(self.save_combobox, wx.SizerFlags().DoubleBorder(wx.ALL).Expand())
        save_sizer.Add(save_notes_label, wx.SizerFlags().DoubleBorder(wx.ALL))
        save_sizer.Add(self.save_notes, wx.SizerFlags().DoubleBorder(wx.ALL).Expand().Proportion(1))
        save_sizer.Add(save_button_sizer, wx.SizerFlags().DoubleBorder(wx.ALL).Expand())
        save_button_sizer.Add(save_button, wx.SizerFlags().Proportion(1))
        save_button_sizer.Add(load_button, wx.SizerFlags().Proportion(1))
        save_button_sizer.Add(delete_button, wx.SizerFlags().Proportion(1))

        launch_button = wx.Button(self, label=_('Launch Game'))

        self.Bind(wx.EVT_BUTTON, self.set_exe, exe_button)
        self.Bind(wx.EVT_BUTTON, self.save, save_button)
        self.Bind(wx.EVT_BUTTON, self.load, load_button)
        self.Bind(wx.EVT_BUTTON, self.delete, delete_button)
        self.Bind(wx.EVT_BUTTON, self.launch, launch_button)
        self.Bind(wx.EVT_COMBOBOX, self.select_slot, self.save_combobox)

        top_sizer.Add(exe_sizer, wx.SizerFlags().TripleBorder(wx.ALL).Expand())
        top_sizer.Add(save_sizer, wx.SizerFlags().TripleBorder(wx.ALL).Proportion(1).Expand())
        top_sizer.Add(launch_button, wx.SizerFlags().TripleBorder(wx.ALL).Expand())

        self.SetSizer(top_sizer)

    def set_exe(self, event):
        path = wx.FileSelector(
            message=_('Select the Noita executable'),
            default_path=str(Path(self.exe_textbox.Value).parent),
            default_filename='noita.exe',
            wildcard='Noita (noita.exe)|noita.exe|Executables (*.exe)|*.exe|All Files (*)|*',
            flags=wx.FD_OPEN | wx.FD_FILE_MUST_EXIST,
        )
        if not path:
            return

        path = Path(path).resolve(strict=True)

        self.exe_textbox.Value = str(path)

        self.save_config()

    def select_slot(self, event):
        name = self.save_combobox.Value
        if name in self.slots:
            self.save_notes.Value = self.slots[name].notes

    @staticmethod
    @lru_cache()
    def noitasavepath():
        return Path(os.environ['UserProfile']) / 'AppData' / 'LocalLow' / 'Nolla_Games_Noita' / 'save00'

    def save(self, event):
        if self.noitasavepath().exists():
            name = self.save_combobox.Value
            notes = self.save_notes.Value
            if name in self.slots:
                slot = self.slots[name]
                slot.notes = notes
            else:
                slot = Slot(notes=notes)
            zip_path = self.slot_path() / f'{slot.uuid}.zip'
            print(f'Saving data into {zip_path}')
            with safe_write(zip_path, 'wb') as zip:
                with zipfile.ZipFile(zip, 'w', compression=zipfile.ZIP_DEFLATED) as zip_file:
                    for file in self.noitasavepath().glob('**/*'):
                        if file != self.noitasavepath():
                            relative_path = file.relative_to(self.noitasavepath())
                            zip_file.write(file, relative_path)
                            print(f'storing {file} as {relative_path}')

            self.slots[name] = slot
            self.save_combobox.Items = list(self.slots)
            self.save_combobox.Value = name
            self.save_config()
        else:
            wx.GenericMessageDialog(
                parent=self,
                message=_('Could not find Noita save directory.\nHave you played the game yet?'),
                caption=_("Error saving slot"),
                style=wx.OK|wx.ICON_ERROR,
            ).ShowModal()

    def load(self, event):
        name = self.save_combobox.Value
        notes = self.save_notes.Value
        if name in self.slots:
            slot = self.slots[name]

            if self.noitasavepath().exists():
                print(f'deleting {self.noitasavepath()}')
                rmtree(self.noitasavepath())

            self.noitasavepath().mkdir(parents=True, exist_ok=True)
            zip_path = self.slot_path() / f'{slot.uuid}.zip'

            print(f'extracting {zip_path} into {self.noitasavepath()}')

            with zipfile.ZipFile(zip_path, 'r') as zip_file:
                zip_file.extractall(self.noitasavepath())

        else:
            wx.GenericMessageDialog(
                parent=self,
                message=_('Could not find slot to load'),
                caption=_("Error loading slot"),
                style=wx.OK|wx.ICON_ERROR,
            ).ShowModal()

    def delete(self, event):
        name = self.save_combobox.Value
        if name in self.slots:
            dialog = wx.GenericMessageDialog(
                parent=self,
                message=_('Are you sure you want to delete slot {name!r}?').format(name=name),
                caption=_("Delete slot {name!r}?").format(name=name),
                style=wx.YES_NO|wx.ICON_WARNING,
            )

            if dialog.ShowModal() == wx.ID_YES:
                slot = self.slots[name]
                del self.slots[name]

                try:
                    (self.slot_path() / f'{slot.uuid}.zip').unlink()
                except IOError:
                    pass

                self.save_combobox.Items = list(self.slots)
                self.save_combobox.Value = ''
                self.save_notes.Value = ''

                self.save_config()

        else:
            wx.GenericMessageDialog(
                parent=self,
                message=_('Could not find slot to delete'),
                caption=_("Error deleting slot"),
                style=wx.OK|wx.ICON_ERROR,
            ).ShowModal()

    def launch(self, event):
        cwd = os.getcwd()
        os.chdir(str(Path(self.exe_textbox.Value).parent))
        subprocess.run(self.exe_textbox.Value)
        os.chdir(cwd)

    @lru_cache()
    def config_path(self):
        config_path = Path(self.appdirs.user_config_dir) / 'savescummer.json'
        config_path.parent.mkdir(parents=True, exist_ok=True)
        return config_path

    @lru_cache()
    def slot_path(self):
        slot_path = Path(self.appdirs.user_data_dir) / 'slots'
        slot_path.mkdir(parents=True, exist_ok=True)
        return slot_path

    def save_config(self):
        data = {
            'exe': self.exe_textbox.Value,
            'slots': {
                name: {
                    'notes': slot.notes,
                    'uuid': slot.uuid,
                } for name, slot in self.slots.items()
            },
        }

        with safe_write(self.config_path()) as file:
            json.dump(data, file)
